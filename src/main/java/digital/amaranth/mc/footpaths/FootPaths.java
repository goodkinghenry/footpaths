package digital.amaranth.mc.footpaths;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class FootPaths extends JavaPlugin {
    public static FootPaths getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("FootPaths");
        if (plugin == null || !(plugin instanceof FootPaths)) {
            throw new RuntimeException("'FootPaths' not found.");
        }
        
        return ((FootPaths) plugin);
    }
    
    @Override
    public void onEnable() {
        saveDefaultConfig();

	    int STEPS_PER_CHECK = getConfig().getInt("StepsPerCheck");
        
        getServer().
                getPluginManager().
                registerEvents(
                        new Paths(
                                getConfig().getDouble("PathFormationRate") * STEPS_PER_CHECK,
                                STEPS_PER_CHECK
                        ), 
                        this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
