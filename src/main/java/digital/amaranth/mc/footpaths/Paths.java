package digital.amaranth.mc.footpaths;

import java.util.Random;
import java.util.Optional;

import org.bukkit.Material;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.event.player.PlayerMoveEvent;

public class Paths implements Listener {
    private int stepCounter;
    
    private final double PATH_FORMATION_RATE;
    private final int STEPS_PER_CHECK;
    
    public Paths (double pathFormationRate, int stepsPerCheck) {
        this.stepCounter = 0;

        this.PATH_FORMATION_RATE = pathFormationRate;
        this.STEPS_PER_CHECK = stepsPerCheck;
    }
    
    Random pathRandom = new Random();
    
    @EventHandler
    public void onPlayerMoveEvent (PlayerMoveEvent e) {
        if (stepCounter > STEPS_PER_CHECK) {
            stepCounter = 0;
            
            Optional.of(e.getTo()).
                    map(l -> l.getBlock()).
                    filter(b -> !b.equals(e.getFrom().getBlock())).
                    ifPresent(b -> {
                var p = e.getPlayer();
                
                if (!p.isSneaking() && !p.isSwimming() && pathRandom.nextDouble() < PATH_FORMATION_RATE) {
                    Block u = b.getRelative(BlockFace.DOWN);
                    
                    switch (u.getType()) {
                        case GRASS_BLOCK -> u.setType(Material.DIRT);
                        case FARMLAND -> u.setType(Material.DIRT);
                        case PODZOL -> u.setType(Material.COARSE_DIRT);
                        case DIRT -> u.setType(Material.COARSE_DIRT);
                        case COARSE_DIRT -> u.setType(Material.DIRT_PATH);
                        default -> {}
                    }
                }
            });
        } else {
            stepCounter++;
        }
    }
}
